#include <gtest/gtest.h>

#include <datatype_converter.hpp>
namespace
{
std::vector<std::string> gtest_args;
class TestClass : public ::testing::Test
{
protected:
    static void SetUpTestCase() {}
    static void TearDownTestCase() {}
    virtual void SetUp() {}
    virtual void TearDown() {}
};
}  // namespace

TEST_F(TestClass, ToValue_int)
{
    int val_int = 0;
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_int, "0"));
    ASSERT_EQ(val_int, 0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_int, "123456"));
    ASSERT_EQ(val_int, 123456);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_int, "-123456"));
    ASSERT_EQ(val_int, -123456);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_int, "1.2"));
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_int, "1.1e10"));
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_int, "2147483647"));
    ASSERT_EQ(val_int, 2147483647);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_int, "-2147483648"));
    ASSERT_EQ(val_int, -2147483648);
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_int, "2147483648"));   // overflow
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_int, "-2147483649"));  // overflow
}
TEST_F(TestClass, ToValue_unsigned)
{
    unsigned val_ui = 0;
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_ui, "0"));
    ASSERT_EQ(val_ui, 0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_ui, "123456"));
    ASSERT_EQ(val_ui, 123456);
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_ui, "-123456"));
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_ui, "1.2"));
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_ui, "1.1e10"));
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_ui, "4294967295"));
    ASSERT_EQ(val_ui, 4294967295);
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_ui, "4294967296"));  // overflow
}
TEST_F(TestClass, ToValue_long)
{
    long val_long = 0;
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_long, "0"));
    ASSERT_EQ(val_long, 0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_long, "123456"));
    ASSERT_EQ(val_long, 123456);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_long, "-123456"));
    ASSERT_EQ(val_long, -123456);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_long, "1.2"));
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_long, "1.1e10"));
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_long, "9223372036854775807"));
    ASSERT_EQ(val_long, 9223372036854775807);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_long, "-9223372036854775808"));
    ASSERT_EQ(val_long, -9223372036854775807 - 1);
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_long, "9223372036854775808"));  // overflow
    ASSERT_FALSE(
        utility::DataTypeConverter::ToValue(val_long, "-9223372036854775809"));  // overflow
}
TEST_F(TestClass, ToValue_float)
{
    float val_float = 0.0e0;
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_float, "123456"));
    ASSERT_FLOAT_EQ(val_float, 123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_float, "+123456"));
    ASSERT_FLOAT_EQ(val_float, 123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_float, "-123456"));
    ASSERT_FLOAT_EQ(val_float, -123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_float, "1.1e0"));
    ASSERT_FLOAT_EQ(val_float, 1.1e0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_float, "-1.1e0"));
    ASSERT_FLOAT_EQ(val_float, -1.1e0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_float, "3.402823e+38"));
    ASSERT_FLOAT_EQ(val_float, 3.402823e+38);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_float, "-3.402823e+38"));
    ASSERT_FLOAT_EQ(val_float, -3.402823e+38);
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_float, "3.402824e+38"));   // overflow
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_float, "-3.402824e+38"));  // overflow
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_float, "-1.1e1000"));      // overflow
}
TEST_F(TestClass, ToValue_double)
{
    double val_double = 0.0e0;
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_double, "123456"));
    ASSERT_DOUBLE_EQ(val_double, 123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_double, "+123456"));
    ASSERT_DOUBLE_EQ(val_double, 123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_double, "-123456"));
    ASSERT_DOUBLE_EQ(val_double, -123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_double, "1.1e0"));
    ASSERT_DOUBLE_EQ(val_double, 1.1e0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_double, "-1.1e0"));
    ASSERT_DOUBLE_EQ(val_double, -1.1e0);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_double, "1.797693e+308"));
    ASSERT_DOUBLE_EQ(val_double, 1.797693e+308);
    ASSERT_TRUE(utility::DataTypeConverter::ToValue(val_double, "-1.797693e+308"));
    ASSERT_DOUBLE_EQ(val_double, -1.797693e+308);
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_double,
                                                     "1.797694e+308"));  // overflow
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_double,
                                                     "-1.797694e+308"));         // overflow
    ASSERT_FALSE(utility::DataTypeConverter::ToValue(val_double, "-1.1e1000"));  // overflow
}

TEST_F(TestClass, ToCorrespondingValue_int)
{
    int val_int = 0;
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_int, "0"));
    ASSERT_EQ(val_int, 0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_int, "123456"));
    ASSERT_EQ(val_int, 123456);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_int, "-123456"));
    ASSERT_EQ(val_int, -123456);
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_int, "1.2"));
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_int, "1.1e10"));
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_int, "2147483647"));
    ASSERT_EQ(val_int, 2147483647);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_int, "-2147483648"));
    ASSERT_EQ(val_int, -2147483648);
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_int, "2147483648"));
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_int, "-2147483649"));
}
TEST_F(TestClass, ToCorrespondingValue_unsigned)
{
    unsigned val_ui = 0;
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_ui, "0"));
    ASSERT_EQ(val_ui, 0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_ui, "123456"));
    ASSERT_EQ(val_ui, 123456);
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_ui, "-123456"));
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_ui, "1.2"));
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_ui, "1.1e10"));
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_ui, "4294967295"));
    ASSERT_EQ(val_ui, 4294967295);
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_ui, "4294967296"));
}
TEST_F(TestClass, ToCorrespondingValue_long)
{
    long val_long = 0;
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_long, "0"));
    ASSERT_EQ(val_long, 0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_long, "123456"));
    ASSERT_EQ(val_long, 123456);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_long, "-123456"));
    ASSERT_EQ(val_long, -123456);
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_long, "1.2"));
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_long, "1.1e10"));
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_long, "9223372036854775807"));
    ASSERT_EQ(val_long, 9223372036854775807);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_long, "-9223372036854775808"));
    ASSERT_EQ(val_long, -9223372036854775807 - 1);
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_long, "9223372036854775808"));
    ASSERT_FALSE(
        utility::DataTypeConverter::ToCorrespondingValue(val_long, "-9223372036854775809"));
}
TEST_F(TestClass, ToCorrespondingValue_float)
{
    float val_float = 0.0e0;
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_float, "123456"));
    ASSERT_FLOAT_EQ(val_float, 123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_float, "+123456"));
    ASSERT_FLOAT_EQ(val_float, 123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_float, "-123456"));
    ASSERT_FLOAT_EQ(val_float, -123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_float, "1.1e0"));
    ASSERT_FLOAT_EQ(val_float, 1.1e0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_float, "-1.1e0"));
    ASSERT_FLOAT_EQ(val_float, -1.1e0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_float, "3.402823e+38"));
    ASSERT_FLOAT_EQ(val_float, 3.402823e+38);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_float, "-3.402823e+38"));
    ASSERT_FLOAT_EQ(val_float, -3.402823e+38);
    ASSERT_FALSE(
        utility::DataTypeConverter::ToCorrespondingValue(val_float, "3.402824e+38"));  // overflow
    ASSERT_FALSE(
        utility::DataTypeConverter::ToCorrespondingValue(val_float, "-3.402824e+38"));  // overflow
    ASSERT_FALSE(
        utility::DataTypeConverter::ToCorrespondingValue(val_float, "-1.1e1000"));  // overflow
}
TEST_F(TestClass, ToCorrespondingValue_double)
{
    double val_double = 0.0e0;
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_double, "123456"));
    ASSERT_DOUBLE_EQ(val_double, 123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_double, "+123456"));
    ASSERT_DOUBLE_EQ(val_double, 123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_double, "-123456"));
    ASSERT_DOUBLE_EQ(val_double, -123456.0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_double, "1.1e0"));
    ASSERT_DOUBLE_EQ(val_double, 1.1e0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_double, "-1.1e0"));
    ASSERT_DOUBLE_EQ(val_double, -1.1e0);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_double, "1.797693e+308"));
    ASSERT_DOUBLE_EQ(val_double, 1.797693e+308);
    ASSERT_TRUE(utility::DataTypeConverter::ToCorrespondingValue(val_double, "-1.797693e+308"));
    ASSERT_DOUBLE_EQ(val_double, -1.797693e+308);
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_double,
                                                                  "1.797694e+308"));  // overflow
    ASSERT_FALSE(utility::DataTypeConverter::ToCorrespondingValue(val_double,
                                                                  "-1.797694e+308"));  // overflow
    ASSERT_FALSE(
        utility::DataTypeConverter::ToCorrespondingValue(val_double, "-1.1e1000"));  // overflow
}