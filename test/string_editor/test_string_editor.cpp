#include <gtest/gtest.h>

#include <string_editor.hpp>

namespace
{
std::vector<std::string> gtest_args;
class TestClass : public ::testing::Test
{
protected:
    static void SetUpTestCase() {}
    static void TearDownTestCase() {}
    virtual void SetUp() {}
    virtual void TearDown() {}
};
}  // namespace

TEST_F(TestClass, split_comma)
{
    {  // case 1
        std::string target  = "a,b,c";
        auto splited_target = utility::StringEditor::Split(target, ',');
        ASSERT_EQ(splited_target.size(), 3);
        ASSERT_EQ(splited_target[0], "a");
        ASSERT_EQ(splited_target[1], "b");
        ASSERT_EQ(splited_target[2], "c");
    }  // case 1
    {  // case 2
        std::string target  = "a,b,c,";
        auto splited_target = utility::StringEditor::Split(target, ',');
        ASSERT_EQ(splited_target.size(), 3);
        ASSERT_EQ(splited_target[0], "a");
        ASSERT_EQ(splited_target[1], "b");
        ASSERT_EQ(splited_target[2], "c");
    }  // case 2
    {  // case 3
        std::string target  = "mafiweo,faoeijaowie,ffaoiem,,efaiwweo,,,,";
        auto splited_target = utility::StringEditor::Split(target, ',');
        ASSERT_EQ(splited_target.size(), 8);
        ASSERT_EQ(splited_target[0], "mafiweo");
        ASSERT_EQ(splited_target[1], "faoeijaowie");
        ASSERT_EQ(splited_target[2], "ffaoiem");
        ASSERT_EQ(splited_target[3], "");
        ASSERT_EQ(splited_target[4], "efaiwweo");
        ASSERT_EQ(splited_target[5], "");
        ASSERT_EQ(splited_target[6], "");
        ASSERT_EQ(splited_target[7], "");
    }  // case 3
}
TEST_F(TestClass, split_space)
{
    std::string target  = "apewja faeopw   awepifjiefjp eaifj e ae";
    auto splited_target = utility::StringEditor::Split(target, ' ');
    ASSERT_EQ(splited_target.size(), 8);
    ASSERT_EQ(splited_target[0], "apewja");
    ASSERT_EQ(splited_target[1], "faeopw");
    ASSERT_EQ(splited_target[2], "");
    ASSERT_EQ(splited_target[3], "");
    ASSERT_EQ(splited_target[4], "awepifjiefjp");
    ASSERT_EQ(splited_target[5], "eaifj");
    ASSERT_EQ(splited_target[6], "e");
    ASSERT_EQ(splited_target[7], "ae");
}
TEST_F(TestClass, trim_right_char)
{
    {  //
        std::string target = "abc";
        ASSERT_EQ(utility::StringEditor::TrimRight(target, 'c'), "ab");
        ASSERT_EQ(utility::StringEditor::TrimRight(target, 'b'), "abc");
    }  //
    {  // local scope
        std::string target = "aaabcccc";
        ASSERT_EQ(utility::StringEditor::TrimRight(target, 'c'), "aaab");
        ASSERT_EQ(utility::StringEditor::TrimRight(target, 'b'), "aaabcccc");
    }  // local scope
}
TEST_F(TestClass, trim_right_string)
{
    {  //
        std::string target = "abc";
        ASSERT_EQ(utility::StringEditor::TrimRight(target, "bc"), "a");
        ASSERT_EQ(utility::StringEditor::TrimRight(target, "c"), "ab");
    }  //
    {  //
        std::string target = "asdfbc.csv";
        ASSERT_EQ(utility::StringEditor::TrimRight(target, ".csv"), "asdfbc");
        ASSERT_EQ(utility::StringEditor::TrimRight(target, "dfbc.csv"), "as");
    }  //
}
TEST_F(TestClass, trim_left_char)
{
    {  //
        std::string target = "abc";
        ASSERT_EQ(utility::StringEditor::TrimLeft(target, 'a'), "bc");
        ASSERT_EQ(utility::StringEditor::TrimLeft(target, 'b'), "abc");
    }  //
    {  // local scope
        std::string target = "aaabcccc";
        ASSERT_EQ(utility::StringEditor::TrimLeft(target, 'a'), "bcccc");
        ASSERT_EQ(utility::StringEditor::TrimLeft(target, 'b'), "aaabcccc");
    }  // local scope
}
TEST_F(TestClass, trim_left_string)
{
    {  //
        std::string target = "abc";
        ASSERT_EQ(utility::StringEditor::TrimLeft(target, "ab"), "c");
        ASSERT_EQ(utility::StringEditor::TrimLeft(target, "a"), "bc");
    }  //
    {  //
        std::string target = "asdfbc.csv";
        ASSERT_EQ(utility::StringEditor::TrimLeft(target, "asdf"), "bc.csv");
        ASSERT_EQ(utility::StringEditor::TrimLeft(target, "asdfbc.csv"), "");
    }  //
}
TEST_F(TestClass, trim_both_chara)
{
    {  //
        std::string target = "abc";
        ASSERT_EQ(utility::StringEditor::TrimBoth(target, 'a'), "bc");
        ASSERT_EQ(utility::StringEditor::TrimBoth(target, 'b'), "abc");
    }  //
    {  //
        std::string target = "aaabccccaaaa";
        ASSERT_EQ(utility::StringEditor::TrimBoth(target, 'a'), "bcccc");
    }  //
}
TEST_F(TestClass, trim_both_string)
{
    {  //
        std::string target = "abcabca";
        ASSERT_EQ(utility::StringEditor::TrimBoth(target, "a"), "bcabc");
        ASSERT_EQ(utility::StringEditor::TrimBoth(target, "abca"), "bca");
    }  //
    {  //
        std::string target = "abcabcabc";
        ASSERT_EQ(utility::StringEditor::TrimBoth(target, "abc"), "abc");
    }  //
}
TEST_F(TestClass, Remove)
{
    namespace m_util = utility;
    std::string str  = "123456789";
    ASSERT_EQ(m_util::StringEditor::Remove(str, '1'), "23456789");
    ASSERT_EQ(m_util::StringEditor::Remove(str, '2'), "13456789");
    ASSERT_EQ(m_util::StringEditor::Remove(str, '3'), "12456789");
    ASSERT_EQ(m_util::StringEditor::Remove(str, '4'), "12356789");
    ASSERT_EQ(m_util::StringEditor::Remove(str, '5'), "12346789");
    ASSERT_EQ(m_util::StringEditor::Remove(str, '6'), "12345789");
    ASSERT_EQ(m_util::StringEditor::Remove(str, '7'), "12345689");
    ASSERT_EQ(m_util::StringEditor::Remove(str, '8'), "12345679");
    ASSERT_EQ(m_util::StringEditor::Remove(str, '9'), "12345678");
    ASSERT_EQ(m_util::StringEditor::Remove(str, 'a'), "123456789");
}