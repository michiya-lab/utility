#include <gtest/gtest.h>

#include <utility.hpp>

namespace
{
std::vector<std::string> gtest_args;
class TestClass : public ::testing::Test
{
protected:
    static void SetUpTestCase() {}
    static void TearDownTestCase() {}
    virtual void SetUp() {}
    virtual void TearDown() {}
};
}  // namespace

TEST_F(TestClass, IsNumber)
{
    // TRUE
    // integer
    ASSERT_TRUE(utility::IsNumber("0"));
    ASSERT_TRUE(utility::IsNumber("1"));
    ASSERT_TRUE(utility::IsNumber("+1"));
    ASSERT_TRUE(utility::IsNumber("-1"));
    ASSERT_TRUE(utility::IsNumber("123"));
    ASSERT_TRUE(utility::IsNumber("+123"));
    ASSERT_TRUE(utility::IsNumber("-123"));
    ASSERT_TRUE(utility::IsNumber("0000123456789"));
    ASSERT_TRUE(utility::IsNumber("+0000123456789"));
    ASSERT_TRUE(utility::IsNumber("-0000123456789"));
    // real
    ASSERT_TRUE(utility::IsNumber("1.2"));
    ASSERT_TRUE(utility::IsNumber("+1.2"));
    ASSERT_TRUE(utility::IsNumber("-1.2"));
    ASSERT_TRUE(utility::IsNumber("1.20000"));
    ASSERT_TRUE(utility::IsNumber("+1.20000"));
    ASSERT_TRUE(utility::IsNumber("-1.20000"));
    ASSERT_TRUE(utility::IsNumber("0001.2"));
    ASSERT_TRUE(utility::IsNumber("+00001.2"));
    ASSERT_TRUE(utility::IsNumber("-00001.2"));
    ASSERT_TRUE(utility::IsNumber("0001.20000"));
    ASSERT_TRUE(utility::IsNumber("+00001.20000"));
    ASSERT_TRUE(utility::IsNumber("-00001.20000"));
    // exponential
    ASSERT_TRUE(utility::IsNumber("1.2e1"));
    ASSERT_TRUE(utility::IsNumber("1.2e+1"));
    ASSERT_TRUE(utility::IsNumber("1.2e-1"));
    ASSERT_TRUE(utility::IsNumber("1.2E1"));
    ASSERT_TRUE(utility::IsNumber("1.2E+1"));
    ASSERT_TRUE(utility::IsNumber("1.2E-1"));
    ASSERT_TRUE(utility::IsNumber("1.2e10000"));
    ASSERT_TRUE(utility::IsNumber("1.2e+10000"));
    ASSERT_TRUE(utility::IsNumber("1.2e-10000"));
    ASSERT_TRUE(utility::IsNumber("1.2e00001"));
    ASSERT_TRUE(utility::IsNumber("1.2e+00001"));
    ASSERT_TRUE(utility::IsNumber("1.2e-00001"));
    ASSERT_TRUE(utility::IsNumber("00001.2e00001"));
    ASSERT_TRUE(utility::IsNumber("00001.2e+00001"));
    ASSERT_TRUE(utility::IsNumber("00001.2e-00001"));
    ASSERT_TRUE(utility::IsNumber("00001.2e1"));
    ASSERT_TRUE(utility::IsNumber("00001.2e+1"));
    ASSERT_TRUE(utility::IsNumber("00001.2e-1"));
    ASSERT_TRUE(utility::IsNumber("+1.2e1"));
    ASSERT_TRUE(utility::IsNumber("+1.2e+1"));
    ASSERT_TRUE(utility::IsNumber("+1.2e-1"));
    ASSERT_TRUE(utility::IsNumber("+1.2e1"));
    ASSERT_TRUE(utility::IsNumber("+1.2e+1"));
    ASSERT_TRUE(utility::IsNumber("+1.2e-1"));
    ASSERT_TRUE(utility::IsNumber("-1.2e1"));
    ASSERT_TRUE(utility::IsNumber("-1.2e+1"));
    ASSERT_TRUE(utility::IsNumber("-1.2e-1"));
    // FALSE
    ASSERT_FALSE(utility::IsNumber(""));
    ASSERT_FALSE(utility::IsNumber(" 1"));
    ASSERT_FALSE(utility::IsNumber("1 "));
    ASSERT_FALSE(utility::IsNumber("1,0"));
    ASSERT_FALSE(utility::IsNumber(" "));
    ASSERT_FALSE(utility::IsNumber("O"));
    ASSERT_FALSE(utility::IsNumber("    "));
    ASSERT_FALSE(utility::IsNumber("1."));
    ASSERT_FALSE(utility::IsNumber("1.."));
    ASSERT_FALSE(utility::IsNumber(".1"));
    ASSERT_FALSE(utility::IsNumber("+.1"));
    ASSERT_FALSE(utility::IsNumber("-.1"));
    ASSERT_FALSE(utility::IsNumber(".1e0"));
    ASSERT_FALSE(utility::IsNumber("0.1e"));
    ASSERT_FALSE(utility::IsNumber("0.1E"));
    ASSERT_FALSE(utility::IsNumber("0.1E0E"));
    ASSERT_FALSE(utility::IsNumber("0.e"));
}
TEST_F(TestClass, IsIntNumber)
{
    // TRUE
    // integer
    ASSERT_TRUE(utility::IsIntNumber("0"));
    ASSERT_TRUE(utility::IsIntNumber("1"));
    ASSERT_TRUE(utility::IsIntNumber("+1"));
    ASSERT_TRUE(utility::IsIntNumber("-1"));
    ASSERT_TRUE(utility::IsIntNumber("123"));
    ASSERT_TRUE(utility::IsIntNumber("+123"));
    ASSERT_TRUE(utility::IsIntNumber("-123"));
    ASSERT_TRUE(utility::IsIntNumber("0000123456789"));
    ASSERT_TRUE(utility::IsIntNumber("+0000123456789"));
    ASSERT_TRUE(utility::IsIntNumber("-0000123456789"));
    // real
    ASSERT_FALSE(utility::IsIntNumber("1.2"));
    ASSERT_FALSE(utility::IsIntNumber("+1.2"));
    ASSERT_FALSE(utility::IsIntNumber("-1.2"));
    ASSERT_FALSE(utility::IsIntNumber("1.20000"));
    ASSERT_FALSE(utility::IsIntNumber("+1.20000"));
    ASSERT_FALSE(utility::IsIntNumber("-1.20000"));
    ASSERT_FALSE(utility::IsIntNumber("0001.2"));
    ASSERT_FALSE(utility::IsIntNumber("+00001.2"));
    ASSERT_FALSE(utility::IsIntNumber("-00001.2"));
    ASSERT_FALSE(utility::IsIntNumber("0001.20000"));
    ASSERT_FALSE(utility::IsIntNumber("+00001.20000"));
    ASSERT_FALSE(utility::IsIntNumber("-00001.20000"));
    // exponential
    ASSERT_FALSE(utility::IsIntNumber("1.2e1"));
    ASSERT_FALSE(utility::IsIntNumber("1.2e+1"));
    ASSERT_FALSE(utility::IsIntNumber("1.2e-1"));
    ASSERT_FALSE(utility::IsIntNumber("1.2E1"));
    ASSERT_FALSE(utility::IsIntNumber("1.2E+1"));
    ASSERT_FALSE(utility::IsIntNumber("1.2E-1"));
    ASSERT_FALSE(utility::IsIntNumber("1.2e10000"));
    ASSERT_FALSE(utility::IsIntNumber("1.2e+10000"));
    ASSERT_FALSE(utility::IsIntNumber("1.2e-10000"));
    ASSERT_FALSE(utility::IsIntNumber("1.2e00001"));
    ASSERT_FALSE(utility::IsIntNumber("1.2e+00001"));
    ASSERT_FALSE(utility::IsIntNumber("1.2e-00001"));
    ASSERT_FALSE(utility::IsIntNumber("00001.2e00001"));
    ASSERT_FALSE(utility::IsIntNumber("00001.2e+00001"));
    ASSERT_FALSE(utility::IsIntNumber("00001.2e-00001"));
    ASSERT_FALSE(utility::IsIntNumber("00001.2e1"));
    ASSERT_FALSE(utility::IsIntNumber("00001.2e+1"));
    ASSERT_FALSE(utility::IsIntNumber("00001.2e-1"));
    ASSERT_FALSE(utility::IsIntNumber("+1.2e1"));
    ASSERT_FALSE(utility::IsIntNumber("+1.2e+1"));
    ASSERT_FALSE(utility::IsIntNumber("+1.2e-1"));
    ASSERT_FALSE(utility::IsIntNumber("+1.2e1"));
    ASSERT_FALSE(utility::IsIntNumber("+1.2e+1"));
    ASSERT_FALSE(utility::IsIntNumber("+1.2e-1"));
    ASSERT_FALSE(utility::IsIntNumber("-1.2e1"));
    ASSERT_FALSE(utility::IsIntNumber("-1.2e+1"));
    ASSERT_FALSE(utility::IsIntNumber("-1.2e-1"));
    // FALSE
    ASSERT_FALSE(utility::IsIntNumber(""));
    ASSERT_FALSE(utility::IsIntNumber(" 1"));
    ASSERT_FALSE(utility::IsIntNumber("1 "));
    ASSERT_FALSE(utility::IsIntNumber("1,0"));
    ASSERT_FALSE(utility::IsIntNumber(" "));
    ASSERT_FALSE(utility::IsIntNumber("O"));
    ASSERT_FALSE(utility::IsIntNumber("    "));
    ASSERT_FALSE(utility::IsIntNumber("1."));
    ASSERT_FALSE(utility::IsIntNumber("1.."));
    ASSERT_FALSE(utility::IsIntNumber(".1"));
    ASSERT_FALSE(utility::IsIntNumber("+.1"));
    ASSERT_FALSE(utility::IsIntNumber("-.1"));
    ASSERT_FALSE(utility::IsIntNumber(".1e0"));
    ASSERT_FALSE(utility::IsIntNumber("0.1e"));
    ASSERT_FALSE(utility::IsIntNumber("0.1E"));
    ASSERT_FALSE(utility::IsIntNumber("0.1E0E"));
    ASSERT_FALSE(utility::IsIntNumber("0.e"));
}
TEST_F(TestClass, IsUnsignedIntNumber)
{
    // TRUE
    // integer
    ASSERT_TRUE(utility::IsUnsignedIntNumber("0"));
    ASSERT_TRUE(utility::IsUnsignedIntNumber("1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-1"));
    ASSERT_TRUE(utility::IsUnsignedIntNumber("123"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+123"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-123"));
    ASSERT_TRUE(utility::IsUnsignedIntNumber("0000123456789"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+0000123456789"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-0000123456789"));
    // real
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+1.2"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-1.2"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.20000"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+1.20000"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-1.20000"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("0001.2"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+00001.2"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-00001.2"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("0001.20000"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+00001.20000"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-00001.20000"));
    // exponential
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2e1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2e+1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2e-1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2E1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2E+1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2E-1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2e10000"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2e+10000"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2e-10000"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2e00001"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2e+00001"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.2e-00001"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("00001.2e00001"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("00001.2e+00001"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("00001.2e-00001"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("00001.2e1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("00001.2e+1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("00001.2e-1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+1.2e1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+1.2e+1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+1.2e-1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+1.2e1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+1.2e+1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+1.2e-1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-1.2e1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-1.2e+1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-1.2e-1"));
    // FALSE
    ASSERT_FALSE(utility::IsUnsignedIntNumber(""));
    ASSERT_FALSE(utility::IsUnsignedIntNumber(" 1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1 "));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1,0"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber(" "));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("O"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("    "));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1."));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("1.."));
    ASSERT_FALSE(utility::IsUnsignedIntNumber(".1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("+.1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("-.1"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber(".1e0"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("0.1e"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("0.1E"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("0.1E0E"));
    ASSERT_FALSE(utility::IsUnsignedIntNumber("0.e"));
}
TEST_F(TestClass, IsRealNumber)
{
    // TRUE
    // integer
    ASSERT_FALSE(utility::IsRealNumber("0"));
    ASSERT_FALSE(utility::IsRealNumber("1"));
    ASSERT_FALSE(utility::IsRealNumber("+1"));
    ASSERT_FALSE(utility::IsRealNumber("-1"));
    ASSERT_FALSE(utility::IsRealNumber("123"));
    ASSERT_FALSE(utility::IsRealNumber("+123"));
    ASSERT_FALSE(utility::IsRealNumber("-123"));
    ASSERT_FALSE(utility::IsRealNumber("0000123456789"));
    ASSERT_FALSE(utility::IsRealNumber("+0000123456789"));
    ASSERT_FALSE(utility::IsRealNumber("-0000123456789"));
    // real
    ASSERT_TRUE(utility::IsRealNumber("1.2"));
    ASSERT_TRUE(utility::IsRealNumber("+1.2"));
    ASSERT_TRUE(utility::IsRealNumber("-1.2"));
    ASSERT_TRUE(utility::IsRealNumber("1.20000"));
    ASSERT_TRUE(utility::IsRealNumber("+1.20000"));
    ASSERT_TRUE(utility::IsRealNumber("-1.20000"));
    ASSERT_TRUE(utility::IsRealNumber("0001.2"));
    ASSERT_TRUE(utility::IsRealNumber("+00001.2"));
    ASSERT_TRUE(utility::IsRealNumber("-00001.2"));
    ASSERT_TRUE(utility::IsRealNumber("0001.20000"));
    ASSERT_TRUE(utility::IsRealNumber("+00001.20000"));
    ASSERT_TRUE(utility::IsRealNumber("-00001.20000"));
    // exponential
    ASSERT_TRUE(utility::IsRealNumber("1.2e1"));
    ASSERT_TRUE(utility::IsRealNumber("1.2e+1"));
    ASSERT_TRUE(utility::IsRealNumber("1.2e-1"));
    ASSERT_TRUE(utility::IsRealNumber("1.2E1"));
    ASSERT_TRUE(utility::IsRealNumber("1.2E+1"));
    ASSERT_TRUE(utility::IsRealNumber("1.2E-1"));
    ASSERT_TRUE(utility::IsRealNumber("1.2e10000"));
    ASSERT_TRUE(utility::IsRealNumber("1.2e+10000"));
    ASSERT_TRUE(utility::IsRealNumber("1.2e-10000"));
    ASSERT_TRUE(utility::IsRealNumber("1.2e00001"));
    ASSERT_TRUE(utility::IsRealNumber("1.2e+00001"));
    ASSERT_TRUE(utility::IsRealNumber("1.2e-00001"));
    ASSERT_TRUE(utility::IsRealNumber("00001.2e00001"));
    ASSERT_TRUE(utility::IsRealNumber("00001.2e+00001"));
    ASSERT_TRUE(utility::IsRealNumber("00001.2e-00001"));
    ASSERT_TRUE(utility::IsRealNumber("00001.2e1"));
    ASSERT_TRUE(utility::IsRealNumber("00001.2e+1"));
    ASSERT_TRUE(utility::IsRealNumber("00001.2e-1"));
    ASSERT_TRUE(utility::IsRealNumber("+1.2e1"));
    ASSERT_TRUE(utility::IsRealNumber("+1.2e+1"));
    ASSERT_TRUE(utility::IsRealNumber("+1.2e-1"));
    ASSERT_TRUE(utility::IsRealNumber("+1.2e1"));
    ASSERT_TRUE(utility::IsRealNumber("+1.2e+1"));
    ASSERT_TRUE(utility::IsRealNumber("+1.2e-1"));
    ASSERT_TRUE(utility::IsRealNumber("-1.2e1"));
    ASSERT_TRUE(utility::IsRealNumber("-1.2e+1"));
    ASSERT_TRUE(utility::IsRealNumber("-1.2e-1"));
    // FALSE
    ASSERT_FALSE(utility::IsRealNumber(""));
    ASSERT_FALSE(utility::IsRealNumber(" 1"));
    ASSERT_FALSE(utility::IsRealNumber("1 "));
    ASSERT_FALSE(utility::IsRealNumber("1,0"));
    ASSERT_FALSE(utility::IsRealNumber(" "));
    ASSERT_FALSE(utility::IsRealNumber("O"));
    ASSERT_FALSE(utility::IsRealNumber("    "));
    ASSERT_FALSE(utility::IsRealNumber("1."));
    ASSERT_FALSE(utility::IsRealNumber("1.."));
    ASSERT_FALSE(utility::IsRealNumber(".1"));
    ASSERT_FALSE(utility::IsRealNumber("+.1"));
    ASSERT_FALSE(utility::IsRealNumber("-.1"));
    ASSERT_FALSE(utility::IsRealNumber(".1e0"));
    ASSERT_FALSE(utility::IsRealNumber("0.1e"));
    ASSERT_FALSE(utility::IsRealNumber("0.1E"));
    ASSERT_FALSE(utility::IsRealNumber("0.1E0E"));
    ASSERT_FALSE(utility::IsRealNumber("0.e"));
}
TEST_F(TestClass, IsUnsignedRealNumber)
{
    // TRUE
    // integer
    ASSERT_FALSE(utility::IsUnsignedRealNumber("0"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("123"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+123"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-123"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("0000123456789"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+0000123456789"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-0000123456789"));
    // real
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+1.2"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-1.2"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.20000"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+1.20000"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-1.20000"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("0001.2"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+00001.2"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-00001.2"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("0001.20000"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+00001.20000"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-00001.20000"));
    // exponential
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2e1"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2e+1"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2e-1"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2E1"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2E+1"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2E-1"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2e10000"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2e+10000"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2e-10000"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2e00001"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2e+00001"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("1.2e-00001"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("00001.2e00001"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("00001.2e+00001"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("00001.2e-00001"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("00001.2e1"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("00001.2e+1"));
    ASSERT_TRUE(utility::IsUnsignedRealNumber("00001.2e-1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+1.2e1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+1.2e+1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+1.2e-1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+1.2e1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+1.2e+1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+1.2e-1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-1.2e1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-1.2e+1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-1.2e-1"));
    // FALSE
    ASSERT_FALSE(utility::IsUnsignedRealNumber(""));
    ASSERT_FALSE(utility::IsUnsignedRealNumber(" 1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("1 "));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("1,0"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber(" "));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("O"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("    "));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("1."));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("1.."));
    ASSERT_FALSE(utility::IsUnsignedRealNumber(".1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("+.1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("-.1"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber(".1e0"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("0.1e"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("0.1E"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("0.1E0E"));
    ASSERT_FALSE(utility::IsUnsignedRealNumber("0.e"));
}