#include <iostream>

#include <datatype_converter.hpp>
#include <string_editor.hpp>
#include <utility.hpp>

int main()
{
    double val = 0.0e0;
    std::cout << std::boolalpha << utility::DataTypeConverter::ToValue(val, "1.23e10") << std::endl;

    return 0;
}
