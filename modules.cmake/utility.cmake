set_property(GLOBAL PROPERTY PATH_UTILITY_MODULE "${CMAKE_CURRENT_LIST_DIR}")
#
macro(SET_UTILITY_VARIABLES)
    get_property(my_module_path GLOBAL PROPERTY PATH_UTILITY_MODULE)
    if(NOT DEFINED UTILITY_LIB_NAME)
        get_property(
            my_module_path
            GLOBAL PROPERTY
            PATH_UTILITY_MODULE
            )
        set(UTILITY_LIB_NAME "utility")
        set(
            UTILITY_SOURCE_DIR
            "${my_module_path}/../src/"
            )
        set(
            UTILITY_INCLUDE_DIR
            "${my_module_path}/../src/"
            )
    endif()
endmacro()
#
macro(LOAD_DEPENDENCIES_UTILITY_MODULE MACRO_ARG)
    SET_UTILITY_VARIABLES()
    add_dependencies(${MACRO_ARG} ${UTILITY_LIB_NAME})
endmacro()
#
macro(LOAD_LINKS_UTILITY_MODULE MACRO_ARG)
    SET_UTILITY_VARIABLES()
    target_link_libraries(
        ${MACRO_ARG}
        PRIVATE ${UTILITY_LIB_NAME}
        )
endmacro()
#
macro(LOAD_INCLUDES_UTILITY_MODULE MACRO_ARG)
    SET_UTILITY_VARIABLES()
    target_include_directories(
        ${MACRO_ARG}
        PRIVATE ${UTILITY_INCLUDE_DIR}
    )
endmacro()
#
macro(LOAD_SETTING_UTILITY_MODULE MACRO_ARG)
    LOAD_DEPENDENCIES_UTILITY_MODULE(${MACRO_ARG})
    LOAD_LINKS_UTILITY_MODULE(${MACRO_ARG})
    LOAD_INCLUDES_UTILITY_MODULE(${MACRO_ARG})
endmacro()
#
macro(GENERATE_LIBRARY_UTILITY_MODULE)
    get_property(IsBuilt GLOBAL PROPERTY IS_FIRST_BUILD_UTILITY_MODULE)
    if(NOT DEFINED IsBuilt)
        include("${CMAKE_CURRENT_LIST_DIR}/../modules.cmake/macro.cmake")
        SET_UTILITY_VARIABLES()
        CHECK_DIRECTORIES_EXIST(${UTILITY_SOURCE_DIR})
        CHECK_DIRECTORIES_EXIST(${UTILITY_INCLUDE_DIR})
        FIND_CPP(MY_UTILITY_SOURCES "${UTILITY_SOURCE_DIR}")
        FIND_HPP(MY_UTILITY_HEADERS "${UTILITY_INCLUDE_DIR}")
        list(APPEND MY_SOURCES ${MY_UTILITY_SOURCES})
        list(APPEND MY_HEADERS ${MY_UTILITY_HEADERS})
        # add library
        DEFINE_ADD_LIBRARY(${UTILITY_LIB_NAME} "${MY_SOURCES}")
        LOAD_INCLUDES_UTILITY_MODULE(${UTILITY_LIB_NAME})
        # install
        set_target_properties(
            ${UTILITY_LIB_NAME}
            PROPERTIES
            PUBLIC_HEADER "${MY_HEADERS}"
            )
        install(
            TARGETS ${UTILITY_LIB_NAME}
            EXPORT ${UTILITY_LIB_NAME}
            ARCHIVE DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
            LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
            PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_PREFIX}/include
            )
        set_property(GLOBAL PROPERTY IS_FIRST_BUILD_UTILITY_MODULE TRUE)
    endif()
endmacro()
#
macro(LINK_UTILITY_MODULE_AS_SUBMODULE MACRO_ARG)
    LOAD_DEPENDENCIES_UTILITY_MODULE(${MACRO_ARG})
    LOAD_LINKS_UTILITY_MODULE(${MACRO_ARG})
    LOAD_INCLUDES_UTILITY_MODULE(${MACRO_ARG})
endmacro()
