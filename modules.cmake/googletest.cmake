macro(fetch_googletest)
    if(NOT TARGET EXTERNAL_GOOGLETEST_PROJECT)        
        set(GOOGLETEST_C_COMPILER ${CMAKE_C_COMPILER})
        set(GOOGLETEST_CXX_COMPILER ${CMAKE_CXX_COMPILER})
        set(GOOGLETEST_INSTALL_DESTINATION ${CMAKE_BINARY_DIR}/googletest)
        set(GOOGLETEST_ROOT ${GOOGLETEST_INSTALL_DESTINATION})
        set(GOOGLETEST_INCLUDE_DIR ${GOOGLETEST_ROOT}/include)
        set(GOOGLETEST_LIBRARY_DIR ${GOOGLETEST_ROOT}/lib)
        set(GOOGLETEST_LIB gtest;gtest_main;pthread)
        include(ExternalProject)
        ExternalProject_Add(
            EXTERNAL_GOOGLETEST_PROJECT
            GIT_REPOSITORY https://github.com/google/googletest
            GIT_TAG release-1.11.0
            CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${GOOGLETEST_INSTALL_DESTINATION} -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS_INIT} -DCMAKE_C_COMPILER=${GOOGLETEST_C_COMPILER} -DCMAKE_CXX_COMPILER=${GOOGLETEST_CXX_COMPILER}
        )
    endif()
endmacro()
