#ifndef DATATYPE_CONVERTER_HPP_
#define DATATYPE_CONVERTER_HPP_

#include <string>

#include <utility.hpp>

namespace utility
{
class DataTypeConverter
{
public:
    DataTypeConverter() {}
    // ToValue : return true/false when convert to value success/false
    static bool ToValue(int &, const std::string &);
    static bool ToValue(unsigned &, const std::string &);
    static bool ToValue(long &, const std::string &);
    static bool ToValue(unsigned long &, const std::string &);
    static bool ToValue(long long &, const std::string &);
    static bool ToValue(unsigned long long &, const std::string &);
    static bool ToValue(float &, const std::string &);
    static bool ToValue(double &, const std::string &);
    // ToCorrespondingValue : return true/false when convert to value success/false.
    // note: Unlike ToValue, the data type must be same, for instance the float string to int, return false
    static bool ToCorrespondingValue(int &, const std::string &);
    static bool ToCorrespondingValue(unsigned &, const std::string &);
    static bool ToCorrespondingValue(long &, const std::string &);
    static bool ToCorrespondingValue(unsigned long &, const std::string &);
    static bool ToCorrespondingValue(long long &, const std::string &);
    static bool ToCorrespondingValue(unsigned long long &, const std::string &);
    static bool ToCorrespondingValue(float &, const std::string &);
    static bool ToCorrespondingValue(double &, const std::string &);

private:
    /* code */

public:  // oparator
    // destructor
    virtual ~DataTypeConverter() = default;
    // copy constractor
    DataTypeConverter(const DataTypeConverter &) = default;
    // copy assignment operator
    DataTypeConverter &operator=(const DataTypeConverter &) = default;
    // move constractor
    DataTypeConverter(DataTypeConverter &&) = default;
    // move assignment operator
    DataTypeConverter &operator=(DataTypeConverter &&) = default;
};

}  // namespace utility

#endif  // DATATYPE_CONVERTER_HPP_