#ifndef STRING_EDITOR_HPP
#define STRING_EDITOR_HPP

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace utility
{
class StringEditor
{
public:
    StringEditor() {}

    static std::vector<std::string> Split(const std::string &, const char &);
    static std::string TrimRight(const std::string &, const char &);
    static std::string TrimRight(const std::string &, const std::string &);
    static std::string TrimLeft(const std::string &, const char &);
    static std::string TrimLeft(const std::string &, const std::string &);
    static std::string TrimBoth(const std::string &, const char &);
    static std::string TrimBoth(const std::string &, const std::string &);
    static std::string Remove(const std::string &, const char &);

private:
    /* code */

public:  // oparator
    // destructor
    virtual ~StringEditor() = default;
    // copy constractor
    StringEditor(const StringEditor &) = default;
    // copy assignment operator
    StringEditor &operator=(const StringEditor &) = default;
    // move constractor
    StringEditor(StringEditor &&) = default;
    // move assignment operator
    StringEditor &operator=(StringEditor &&) = default;
};
}  // namespace utility

#endif  // STRING_EDITOR_HPP
