#ifndef UTILITY_HPP_
#define UTILITY_HPP_

#include <iostream>
#include <regex>
#include <string>

namespace utility
{
bool IsNumber(const std::string &);
bool IsIntNumber(const std::string &);
bool IsUnsignedIntNumber(const std::string &);
bool IsRealNumber(const std::string &);
bool IsUnsignedRealNumber(const std::string &);

}  // namespace utility

#endif  // UTILITY_HPP_
