#include <utility.hpp>

namespace utility
{
bool IsIntNumber(const std::string &str)
{
    return std::regex_match(str, std::regex("[+-]?\\d+"));
}
bool IsUnsignedIntNumber(const std::string &str)
{
    return std::regex_match(str, std::regex("\\d+"));
}
bool IsRealNumber(const std::string &str)
{
    return std::regex_match(str, std::regex("[+-]?\\d+(\\.\\d+)((e|E)[+-]?\\d+)?"));
}
bool IsUnsignedRealNumber(const std::string &str)
{
    return std::regex_match(str, std::regex("\\d+(\\.\\d+)((e|E)[+-]?\\d+)?"));
}
bool IsNumber(const std::string &str)
{
    return std::regex_match(str, std::regex("[+-]?\\d+(\\.\\d+)?((e|E)[+-]?\\d+)?"));
}
}  // namespace utility
