#include <datatype_converter.hpp>

#include <limits>

namespace std
{
unsigned stou(std::string const &str, size_t *idx = 0, int base = 10)
{
    const unsigned long val = std::stoul(str, idx, base);
    if (std::numeric_limits<unsigned>::max() < val)
    {
        throw std::out_of_range("stou");
    }
    return static_cast<unsigned>(val);
}
}  // namespace std
namespace utility
{
bool DataTypeConverter::ToValue(int &val, const std::string &str)
{
    try
    {
        val = std::stoi(str);
        return true;
    }
    catch (...)
    {
        return false;
    }
}
bool DataTypeConverter::ToValue(unsigned &val, const std::string &str)
{
    try
    {
        val = std::stou(str);
        return true;
    }
    catch (...)
    {
        return false;
    }
}
bool DataTypeConverter::ToValue(long &val, const std::string &str)
{
    try
    {
        val = std::stol(str);
        return true;
    }
    catch (...)
    {
        return false;
    }
}
bool DataTypeConverter::ToValue(unsigned long &val, const std::string &str)
{
    try
    {
        val = std::stoul(str);
        return true;
    }
    catch (...)
    {
        return false;
    }
}

bool DataTypeConverter::ToValue(long long &val, const std::string &str)
{
    try
    {
        val = std::stol(str);
        return true;
    }
    catch (...)
    {
        return false;
    }
}
bool DataTypeConverter::ToValue(unsigned long long &val, const std::string &str)
{
    try
    {
        val = std::stoul(str);
        return true;
    }
    catch (...)
    {
        return false;
    }
}
bool DataTypeConverter::ToValue(float &val, const std::string &str)
{
    try
    {
        val = std::stof(str);
        return true;
    }
    catch (...)
    {
        return false;
    }
}
bool DataTypeConverter::ToValue(double &val, const std::string &str)
{
    try
    {
        val = std::stod(str);
        return true;
    }
    catch (...)
    {
        return false;
    }
}
bool DataTypeConverter::ToCorrespondingValue(int &val, const std::string &str)
{
    if (utility::IsIntNumber(str))
    {
        try
        {
            val = std::stoi(str);
            return true;
        }
        catch (...)
        {
            return false;
        }
    }  // if str
    return false;
}
bool DataTypeConverter::ToCorrespondingValue(unsigned &val, const std::string &str)
{
    if (utility::IsUnsignedIntNumber(str))
    {
        try
        {
            unsigned long val_ul = std::stoul(str);
            if (val_ul <= std::numeric_limits<unsigned>::max())
            {
                val = (unsigned)val_ul;
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (...)
        {
            return false;
        }
    }
    return false;
}
bool DataTypeConverter::ToCorrespondingValue(long &val, const std::string &str)
{
    if (utility::IsIntNumber(str))
    {
        try
        {
            val = std::stol(str);
            return true;
        }
        catch (...)
        {
            return false;
        }
    }
    return false;
}
bool DataTypeConverter::ToCorrespondingValue(unsigned long &val, const std::string &str)
{
    if (utility::IsIntNumber(str))
    {
        try
        {
            val = std::stoul(str);
            return true;
        }
        catch (...)
        {
            return false;
        }
    }
    return false;
}
bool DataTypeConverter::ToCorrespondingValue(float &val, const std::string &str)
{
    if (utility::IsNumber(str))
    {
        try
        {
            val = std::stof(str);
            return true;
        }
        catch (...)
        {
            return false;
        }
    }
    return false;
}
bool DataTypeConverter::ToCorrespondingValue(double &val, const std::string &str)
{
    if (utility::IsNumber(str))
    {
        try
        {
            val = std::stod(str);
            return true;
        }
        catch (...)
        {
            return false;
        }
    }
    return false;
}
}  // namespace utility
