#!/bin/bash

# variables
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
BUILD_DIR=${SCRIPT_DIR}/../build
mkdir -p ${BUILD_DIR}
cd ${BUILD_DIR}

cmake \
-D CMAKE_C_COMPILER=gcc \
-D CMAKE_CXX_COMPILER=g++ \
-D CMAKE_CXX_STANDARD=14 \
-D CMAKE_BUILD_TYPE:STRING=Release \
-D BUILD_TESTING=ON \
-D BUILD_EXAMPLES=ON \
-D CMAKE_INSTALL_PREFIX="./lib_installed" \
-D CMAKE_CXX_FLAGS:STRING="-Wall -Wextra -Wpedantic -Werror" \
-D CMAKE_CXX_FLAGS_RELEASE:STRING="-O2 -DNDEBUG -march=native -mtune=native" \
-D CMAKE_VERBOSE_MAKEFILE=ON \
../

make