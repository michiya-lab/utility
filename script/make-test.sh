#!/bin/bash
# variables
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
BUILD_DIR=${SCRIPT_DIR}/../build
# make
cd ${BUILD_DIR}
make test
