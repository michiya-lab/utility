FROM debian:latest

RUN apt-get -yq update
RUN apt-get -yq install build-essential git cmake

# Clean
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*
